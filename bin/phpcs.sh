#!/usr/bin/env bash

[[ -z $1 ]] && echo "Veuillez spécifier la version de PHP désirée (ex: 7.1)." && exit 1

DIR=$(cd `dirname $0` && pwd)
DIRNAME=$(basename ${DIR})

cd ${DIR}/..

version=$1

# conversion de la version spécifiée au format PHP_VERSION_ID, ex: "7.2" devien "70200"
a=(${version//./ })
PHP_VERSION_ID=$((a[0]*10000+a[1]*100+a[2]))

phpcs="./vendor/bin/phpcs"
libdir="./vendor/unicaen"

for path in ${libdir}/*
do
    dirname=$(basename -- "$path")
    echo "Processing '${path}' against PHP version ${version} (PHP_VERSION_ID=${PHP_VERSION_ID})"
    echo
    ${phpcs} \
        -p \
         --standard=PHPCompatibility \
        --runtime-set php_version ${PHP_VERSION_ID} \
        ${path}
done
