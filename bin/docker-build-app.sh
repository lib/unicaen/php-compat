#!/usr/bin/env bash

DIR=$(cd `dirname $0` && pwd)

cd ${DIR}/..

# Build des images

v=7.0 ; docker build --rm -t skeleton-app-${v} -f Dockerfile-${v} .
v=7.0 ; docker rm -f skeleton-app-${v}-container
v=7.0 ; docker run -d -p 8070:80 -p 8470:443 -v ${PWD}/composer.json:/app/composer.json -v ${PWD}/.phan:/app/.phan --name skeleton-app-${v}-container skeleton-app-${v}
v=7.0 ; docker exec skeleton-app-${v}-container composer update --no-suggest --optimize-autoloader

v=7.1 ; docker build --rm -t skeleton-app-${v} -f Dockerfile-${v} .
v=7.1 ; docker rm -f skeleton-app-${v}-container
v=7.1 ; docker run -d -p 8071:80 -p 8471:443 -v ${PWD}/composer.json:/app/composer.json -v ${PWD}/.phan:/app/.phan --name skeleton-app-${v}-container skeleton-app-7.1
v=7.1 ; docker exec skeleton-app-${v}-container composer update --no-suggest --optimize-autoloader

v=7.2 ; docker build --rm -t skeleton-app-${v} -f Dockerfile-${v} .
v=7.2 ; docker rm -f skeleton-app-${v}-container
v=7.2 ; docker run -d -p 8072:80 -p 8472:443 -v ${PWD}/composer.json:/app/composer.json -v ${PWD}/.phan:/app/.phan --name skeleton-app-${v}-container skeleton-app-${v}
v=7.2 ; docker exec skeleton-app-${v}-container composer update --no-suggest --optimize-autoloader

v=7.3 ; docker build --rm -t skeleton-app-${v} -f Dockerfile-${v} .
v=7.3 ; docker rm -f skeleton-app-${v}-container
v=7.3 ; docker run -d -p 8073:80 -p 8473:443 -v ${PWD}/composer.json:/app/composer.json -v ${PWD}/.phan:/app/.phan --name skeleton-app-${v}-container skeleton-app-${v}
v=7.3 ; docker exec skeleton-app-${v}-container composer update --no-suggest --optimize-autoloader


# PHAN
## analyse des modules unicaen/* :
v=7.0 ; docker exec skeleton-app-${v}-container vendor/bin/phan --config-file .phan/config-unicaen.php --target-php-version ${v} > /tmp/phan-unicaen-${v}.log
v=7.1 ; docker exec skeleton-app-${v}-container vendor/bin/phan --config-file .phan/config-unicaen.php --target-php-version ${v} > /tmp/phan-unicaen-${v}.log
v=7.2 ; docker exec skeleton-app-${v}-container vendor/bin/phan --config-file .phan/config-unicaen.php --target-php-version ${v} > /tmp/phan-unicaen-${v}.log
v=7.3 ; docker exec skeleton-app-${v}-container vendor/bin/phan --config-file .phan/config-unicaen.php --target-php-version ${v} > /tmp/phan-unicaen-${v}.log
## analyse des vendors :
v=7.0 ; docker exec skeleton-app-${v}-container vendor/bin/phan --config-file .phan/config-vendor.php --target-php-version ${v} > /tmp/phan-vendor-${v}.log
v=7.1 ; docker exec skeleton-app-${v}-container vendor/bin/phan --config-file .phan/config-vendor.php --target-php-version ${v} > /tmp/phan-vendor-${v}.log
v=7.2 ; docker exec skeleton-app-${v}-container vendor/bin/phan --config-file .phan/config-vendor.php --target-php-version ${v} > /tmp/phan-vendor-${v}.log
v=7.3 ; docker exec skeleton-app-${v}-container vendor/bin/phan --config-file .phan/config-vendor.php --target-php-version ${v} > /tmp/phan-vendor-${v}.log

# PHPCompatibility
v=7.0 ; docker exec skeleton-app-${v}-container bin/phpcs.sh ${v}
v=7.1 ; docker exec skeleton-app-${v}-container bin/phpcs.sh ${v}
v=7.2 ; docker exec skeleton-app-${v}-container bin/phpcs.sh ${v}
v=7.3 ; docker exec skeleton-app-${v}-container bin/phpcs.sh ${v}
