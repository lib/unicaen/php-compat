ARG PHP_VERSION
FROM unicaen-dev-php${PHP_VERSION}-apache

LABEL maintainer="Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>"

ARG PHP_VERSION

ADD . /app

ENV APACHE_CONF_DIR=/etc/apache2
ENV PHP_CONF_DIR=/etc/php/${PHP_VERSION}

RUN pecl install ast && \
    echo "extension=ast.so" > ${PHP_CONF_DIR}/cli/conf.d/ast.ini

# PHAN n'aime pas xdebug
RUN phpdismod xdebug

# Nettoyage
RUN apt-get autoremove -y && apt-get clean && rm -rf /tmp/* /var/tmp/*

WORKDIR /app
