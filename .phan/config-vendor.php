<?php

/**
 *
 * Config PHAN pour ...
 *
 */

/**
 * This configuration will be read and overlaid on top of the
 * default configuration. Command line arguments will be applied
 * after this file is read.
 */
return [
    // Supported values: `'5.6'`, `'7.0'`, `'7.1'`, `'7.2'`, `'7.3'`, `null`.
    // If this is set to `null`,
    // then Phan assumes the PHP version which is closest to the minor version
    // of the php executable used to execute Phan.
    //
    // Note that the **only** effect of choosing `'5.6'` is to infer that functions removed in php 7.0 exist.
    // (See `backward_compatibility_checks` for additional options)
    // TODO: Set this.
    'target_php_version' => null,

    // Backwards Compatibility Checking. This is very slow
    // and expensive, but you should consider running
    // it before upgrading your version of PHP from 5.x to 7.0,
    // due to the backward compatibility breaks of parsing in PHP 7.0
    'backward_compatibility_checks' => true,

    // Added in 0.10.0. Set this to false to emit
    // PhanUndeclaredFunction issues for internal functions
    // that Phan has signatures for,
    // but aren't available in the codebase or the
    // internal functions used to run phan
    'ignore_undeclared_functions_with_known_signatures' => false,

    // If empty, no filter against issues types will be applied.
    // If this white-list is non-empty, only issues within the list
    // will be emitted by Phan.
    'whitelist_issue_types' => [
        'PhanDeprecatedFunctionInternal',  // Warns about a few functions deprecated in 7.0 and later.
        'PhanUndeclaredFunction',  // Check for removed functions such as split() that were deprecated in php 5.x and removed in php 7.0.

        'PhanCompatibleExpressionPHP7',
        'PhanCompatibleKeyedArrayAssignPHP70',
        'PhanCompatibleNullableTypePHP70',
        'PhanCompatiblePHP7',  // This only checks for **syntax** where the parsing may have changed. This check is enabled by `backward_compatibility_checks`
        'PhanCompatibleShortArrayAssignPHP70',
        'PhanCompatibleUseIterablePHP71',
        'PhanCompatibleUseObjectPHP71',
        'PhanCompatibleUseObjectPHP71',
        'PhanCompatibleUseVoidPHP70',
        'PhanCompatibleVoidTypePHP70',

//        'PhanDeprecatedClass',
//        'PhanDeprecatedFunction',
//        'PhanDeprecatedInterface',
//        'PhanDeprecatedTrait',
        'PhanParamSignatureMismatch',
//        'PhanParamSignatureRealMismatchHasParamType',
//        'PhanParamSignatureRealMismatchParamType',
        'PhanParamSignatureRealMismatchTooFewParameters',
        'PhanParamSignatureRealMismatchTooFewParametersInternal',
        'PhanParamTooFew',
//        'PhanParamTooMany',
//        'PhanParamTooManyInternal',
//        'PhanSyntaxError',
//        'PhanTypeMismatchArgument',
//        'PhanTypeMismatchArgumentInternal',
//        'PhanTypeMismatchDeclaredParam',
//        'PhanTypeMismatchDeclaredParamNullable',
//        'PhanTypeMismatchDefault',
//        'PhanTypeMismatchForeach',
//        'PhanTypeMismatchProperty',
//        'PhanTypeMismatchReturn',
//        'PhanTypeMissingReturn',
//        'PhanUndeclaredClassInstanceof',
//        'PhanUnreferencedUseNormal',
        'PhanContinueTargetingSwitch', // This detects code causing a warning in PHP 7.3.
    ],

    // A list of directories that should be parsed for class and
    // method information. After excluding the directories
    // defined in exclude_analysis_directory_list, the remaining
    // files will be statically analyzed for errors.
    //
    // Thus, both first-party and third-party code being used by
    // your application should be included in this list.
    'directory_list' => [
        'vendor',
        'vendor/doctrine',
        'vendor/zendframework',
        'vendor/zf-commons',
    ],

    // A directory list that defines files that will be excluded
    // from static analysis, but whose class and method
    // information should be included.
    //
    // Generally, you'll want to include the directories for
    // third-party code (such as "vendor/") in this list.
    //
    // n.b.: If you'd like to parse but not analyze 3rd
    //       party code, directories containing that code
    //       should be added to both the `directory_list`
    //       and `exclude_analysis_directory_list` arrays.
    'exclude_analysis_directory_list' => [
        'vendor/bin',
        'vendor/composer',
        'vendor/container-interop',
        'vendor/dealerdirect',
//        'vendor/doctrine',
        'vendor/easybook',
        'vendor/felixfbecker',
        'vendor/jasig',
        'vendor/microsoft',
        'vendor/mpdf',
        'vendor/myclabs',
        'vendor/netresearch',
        'vendor/paragonie',
        'vendor/phan',
        'vendor/phpcompatibility',
        'vendor/phpdocumentor',
        'vendor/phpspec',
        'vendor/phpunit',
        'vendor/psr',
        'vendor/ramsey',
        'vendor/sabre',
        'vendor/sebastian',
        'vendor/setasign',
        'vendor/slevomat',
        'vendor/squizlabs',
        'vendor/symfony',
        'vendor/true',
        'vendor/unicaen',
        'vendor/webmozart',
//        'vendor/zendframework',
//        'vendor/zf-commons',
    ],
];
